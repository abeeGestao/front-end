import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProdutosResponseModel } from 'app/produtos/models/produto-response.model';
import { PedidoModel } from 'app/pedidos/models/pedido.model';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
import { ConfigService } from 'app/services/config';

@Injectable({
  providedIn: 'root'
})
export class CaixaService {
  
  url : string;
  constructor(private host: ConfigService, private http: HttpClient) { 
    this.url = this.host.getApiBaseURL();
  }

  getAllProdutos(): Observable<ProdutosResponseModel> {
    console.log(this.url);
    return this.http.get<ProdutosResponseModel>(this.url + 'getAllProdutos');
  }

  postPedidos(request: PedidoModel): Observable<RespostaServico>{
    return this.http.post<RespostaServico>(this.url + "cadastroPedidoPdv", request);
  }
  
}
