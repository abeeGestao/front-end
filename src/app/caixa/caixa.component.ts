import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CaixaService } from './caixa.service';
import { startWith, map } from 'rxjs/operators';
import { ProdutosComboModel } from 'app/pedidos/models/produto-combo.model';
import { ProdutosResponseModel } from 'app/produtos/models/produto-response.model';
import { PedidoModel } from 'app/pedidos/models/pedido.model';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
declare var $: any;

@Component({
  selector: 'app-caixa',
  templateUrl: './caixa.component.html',
  styleUrls: ['./caixa.component.css']
})
export class CaixaComponent implements OnInit {
  public ProdutoFormGroup: FormGroup;
  public VendaFormGroup: FormGroup;
  constructor(private service: CaixaService, private formBuilder: FormBuilder) { }
  public opcoesProduto: ProdutosComboModel[] = [new ProdutosComboModel()];
  public opcoesProdutoFiltrado: ProdutosComboModel[];
  public cdProduto: number;
  public quantidadeProduto: number = 0;
  public valorTotaProdutos: number = 0;
  public filtrosGroup: FormGroup;
  public produtoSelecionado: ProdutosComboModel = new ProdutosComboModel();
  public produtosPedidoSelecionados: ProdutosComboModel[] = [];
  public totalBruto = 0;
  public descontoTotal = 0;
  public valorPago = 0;
  public formaPagamento;

  
  @HostListener('window:keydown', ['$event'])
  doSomething($event) {
    this.pressTeclas($event.key);
  }

  ngOnInit(): void {
    this.opcoesProduto[0].cdProduto = 0;
    this.opcoesProduto[0].nome = '';
    this.opcoesProduto[0].cdBarras = '';
    this.opcoesProduto[0].quantidade = 0;
    this.opcoesProduto[0].valor = 0;  
    this.produtoSelecionado.nome = "";
    this.produtoSelecionado.cdBarras = "";
    
    this.getAllProdutos();
    this.ProdutoFormGroup = this.formBuilder.group({
      listaProdutoControl: [null],
      quantidadeProduto: [null],
      codigoBarras: [null]
    });
    this.ProdutoFormGroup.get('listaProdutoControl').valueChanges
    .pipe(
      startWith(''),
      //filtrando todos os produtos que fazem match com o input
      map(value =>  this._filterProduto(value))
    ).subscribe(
      it => {       
        this.opcoesProdutoFiltrado = it    
        
        //buscando somente o produto que condiz com o código digitado no input
        if(this.opcoesProdutoFiltrado.find( e => e.cdProduto == this.ProdutoFormGroup.get("listaProdutoControl").value)){
          this.produtoSelecionado =  this.opcoesProdutoFiltrado.find( e => {
            return e.cdProduto == this.ProdutoFormGroup.get("listaProdutoControl").value
          }
          );
        }
      }
    );
    const element = window.document.getElementById("codigobarras");
    element.focus();
  }

  public addProdutoCodigoBarras(cod :string){
    const prod: ProdutosComboModel = this.opcoesProduto.find(e => e.cdBarras == cod);
    if(prod != null){
      prod.quantidade = 1;
      this.addProdutoLista(prod);
    }else{
      this.showNotification('top', 'center', 'warning', 'Código de Barras não encontrada!');
    }
    this.novoProduto();
  }
  public buscarProdutoCodigoBarras(cod){
    const prod: ProdutosComboModel = this.opcoesProduto.find(e => e.cdBarras == cod);
    if(prod != null){
      this.produtoSelecionado = prod;
      this.novoProduto();
    }
  }

  public buscarProdutoCodigoProdutoEnter(cod){
    const prod: ProdutosComboModel = this.opcoesProduto.find(e => e.cdBarras == cod);
    if(prod != null){
      this.produtoSelecionado = prod;
    }
    this.ProdutoFormGroup.controls['quantidadeProduto'].setValue(1);
    this.ProdutoFormGroup.controls['codigoBarras'].setValue(this.produtoSelecionado.cdBarras);
    const element = window.document.getElementById("quantidadeProduto");
    element.focus();
  }
  public buscarProdutoCodigoProduto(cod){
    const prod: ProdutosComboModel = this.opcoesProduto.find(e => e.cdBarras == cod);
    if(prod != null){
      this.produtoSelecionado = prod;
    }
    this.ProdutoFormGroup.controls['quantidadeProduto'].setValue(1);
    this.ProdutoFormGroup.controls['codigoBarras'].setValue(this.produtoSelecionado.cdBarras);
  }

  public addProdutoSelecionado(quantidade){
    this.produtoSelecionado.quantidade = quantidade;
    this.addProdutoLista(this.produtoSelecionado);
    this.novoProduto();
  }
  
  public novoProduto(){
    this.produtoSelecionado = new ProdutosComboModel();
    this.produtoSelecionado.nome = "";
    this.produtoSelecionado.valor = null;
    this.ProdutoFormGroup.controls['listaProdutoControl'].setValue("");
    this.ProdutoFormGroup.controls['quantidadeProduto'].setValue(null);
    this.ProdutoFormGroup.controls['codigoBarras'].setValue(null);
    const element = window.document.getElementById("codigobarras");
    element.focus();
  }

  public addProdutoLista(produto: ProdutosComboModel){
    const pro = new ProdutosComboModel();
    pro.cdProduto = produto.cdProduto;
    pro.nome = produto.nome;
    pro.cdBarras = produto.cdBarras;
    pro.descricao = produto.descricao;
    pro.status = produto.status;
    pro.valor = produto.valor
    pro.quantidade = produto.quantidade;
    this.produtosPedidoSelecionados.push(pro);
    this.calcularTotalBruto();
  }

  public removeProduto(produto: ProdutosComboModel){
    const index = this.produtosPedidoSelecionados.indexOf( produto );
    this.produtosPedidoSelecionados.splice(index, 1);
  }

  private _filterProduto(valuePro: string): ProdutosComboModel[] {
    const filterProValue = valuePro.toString().toLowerCase();
    return this.opcoesProduto.filter(
      item =>  {
        if(item.cdProduto.toString().includes(filterProValue) || item.nome?.toLowerCase().includes(filterProValue) || item.cdBarras?.toString().includes(filterProValue)){
          return item;
        }
      }
    );
  }

  getAllProdutos() {
    this.service.getAllProdutos().subscribe(
      (produtos: ProdutosResponseModel) => {
        this.opcoesProduto = produtos.listaProduto;
        this.opcoesProdutoFiltrado = produtos.listaProduto;
      }
    );
  }

  public cadastrarPedido() {
    console.log(this.valorPago);
    this.VendaFormGroup = this.formBuilder.group({
      desconto: this.descontoTotal,
      valorPago: this.valorPago,
      formaPagamento: this.formaPagamento,
      listaProduto: [this.produtosPedidoSelecionados]
    });
    console.log(this.VendaFormGroup);
    this.service.postPedidos(this.VendaFormGroup.value).subscribe(
      (response: RespostaServico) => {
        if (response.codigoServico === 0) {
          this.showNotification('top', 'center', 'success', 'Venda concluída');
          this.novaVenda();
        } else {
          this.showNotification('top', 'center', 'warning', 'Erro ao tentar fazer a venda');
        }
      }
    );
  }
  calcularTotalBruto(){
    this.totalBruto = 0;
    this.produtosPedidoSelecionados.forEach(pro => {
      this.totalBruto += pro.valor * pro.quantidade
    });
  }

  novaVenda(){
    this.novoProduto();
    this.produtosPedidoSelecionados = [] ;
    this.totalBruto = 0;
    this.descontoTotal = 0;
    this.formaPagamento = 0;
    this.valorPago = 0;
  }
  pressTeclas(tecla){
    if(tecla == "F2"){
      console.log("f2 press");
      this.novaVenda();
    }if(tecla == "F4"){
      this.novoProduto();
    }if(tecla == "F7"){
      this.cadastrarPedido();
    }if(tecla == "F8"){
      this.novaVenda();;
    }
    
  }
  showNotification(from, align, cor, mensagem) {
    // cor:   '','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: 'notifications',
        message: mensagem

    }, {
        type: cor,
        timer: 50,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }


}
