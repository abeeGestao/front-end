import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PedidoFiltroRequestModel } from 'app/pedidos/models/pedido-filtro-request.model';
import { PedidoResponseModel } from 'app/pedidos/models/pedido-response.model';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
import { ConfigService } from 'app/services/config';



@Injectable({
  providedIn: 'root'
})
export class PedidosService {

  url : string;
  constructor(private host: ConfigService, private http: HttpClient) { 
    this.url = this.host.getApiBaseURL();
  }

  getPedidos(request: PedidoFiltroRequestModel): Observable<PedidoResponseModel> {
    return this.http.post<PedidoResponseModel>(this.url+"getPedidos", request);

  }

  deltePedido(id: number): Observable<RespostaServico>{
    console.log("Requisição Delete, ID:"+id)
    return this.http.delete<RespostaServico>(this.url + "deletePedido/"+id);
  }
}
