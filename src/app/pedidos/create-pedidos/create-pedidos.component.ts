import { Component, OnInit } from '@angular/core';
import { CreatePedidosService } from './create-pedidos.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ClienteResponseModel } from 'app/clientes/models/cliente-response.model';
import { ProdutosResponseModel } from 'app/produtos/models/produto-response.model';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { ClienteComboModel } from '../models/cliente-combo-model';
import { ProdutosComboModel } from '../models/produto-combo.model';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
declare var $: any;



@Component({
  selector: 'app-create-pedidos',
  templateUrl: './create-pedidos.component.html',
  styleUrls: ['./create-pedidos.component.css']
})

export class CreatePedidosComponent implements OnInit {
  
  public opcoesCliente: ClienteComboModel[] = [new ClienteComboModel()];
  public opcoesClienteFiltrado: ClienteComboModel[];
  public opcoesProduto: ProdutosComboModel[] = [new ProdutosComboModel()];
  public opcoesProdutoFiltrado: ProdutosComboModel[];
  public cdProduto: number;
  public quantidadeProduto: number = 0;
  public valorTotaProdutos: number = 0;
  public filtrosGroup: FormGroup;
  public produtoSelecionado: ProdutosComboModel = new ProdutosComboModel();
  public produtosPedidoSelecionados: ProdutosComboModel[] = [];
  constructor(public service: CreatePedidosService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.opcoesCliente[0].cdCliente = 0;
    this.opcoesCliente[0].nome = '';
    this.opcoesProduto[0].cdProduto = 0;
    this.opcoesProduto[0].nome = '';
    this.opcoesProduto[0].cdBarras = '';
    this.opcoesProduto[0].quantidade = 0;
    this.opcoesProduto[0].valor = 0;
    this.produtoSelecionado.valor = 0;
    this.getAllClientes();
    this.getAllProdutos();
    this.filtrosGroup = this.formBuilder.group({
      listaProdutoControl: [null],
      formaPagamento: [null],
      observacao: [null],      
      cdCliente: [null],
      desconto: [null],
      listaProduto: [null],
      quantidade: [null]
    });
    this.filtrosGroup.get('cdCliente').valueChanges
    .pipe(
      startWith(''),
      map(value =>  this._filter(value))
    ).subscribe(
      it=> this.opcoesClienteFiltrado = it
    );
    this.filtrosGroup.get('listaProdutoControl').valueChanges
    .pipe(
      startWith(''),
      map(value =>  this._filterProduto(value))
    ).subscribe(
      it => {        
        this.opcoesProdutoFiltrado = it       
        if(this.opcoesProdutoFiltrado.find( e => e.cdProduto == this.filtrosGroup.get("listaProdutoControl").value)){
          this.produtoSelecionado =  this.opcoesProdutoFiltrado.find( e => e.cdProduto == this.filtrosGroup.get("listaProdutoControl").value);
                 console.log(this.produtoSelecionado);
        }
      }
    );
  }

  public addProdutos(produto: ProdutosComboModel){
    const pro = new ProdutosComboModel();
    pro.cdProduto = produto.cdProduto;
    pro.nome = produto.nome;
    pro.cdBarras = produto.cdBarras;
    pro.descricao = produto.descricao;
    pro.status = produto.status;
    pro.valor = produto.valor
    pro.quantidade = this.filtrosGroup.get('quantidade').value;
    this.produtosPedidoSelecionados.push(pro);
  }

  public removeProduto(produto: ProdutosComboModel){
    const index = this.produtosPedidoSelecionados.indexOf( produto );
    this.produtosPedidoSelecionados.splice(index, 1);
  }

  public cadastrarPedido() {
    this.filtrosGroup.get('listaProduto').setValue(this.produtosPedidoSelecionados);
    console.log(this.filtrosGroup.value);
    this.service.postPedidos(this.filtrosGroup.value).subscribe(
      (response: RespostaServico) => {
        if (response.codigoServico === 0) {
          this.showNotification('top', 'center', 'success', 'Pedido cadastrado com sucesso!');
        } else {
          this.showNotification('top', 'center', 'warning', 'Falha ao tentar cadastrar o pedido!');
        }
      }
    );
  }

  getAllClientes() {
    this.service.getAllClientes().subscribe(
      (clientes: ClienteResponseModel) => {
        this.opcoesCliente = clientes.listaCliente;
        this.opcoesClienteFiltrado = clientes.listaCliente;
      }
    );
  }
  getAllProdutos() {
    this.service.getAllProdutos().subscribe(
      (produtos: ProdutosResponseModel) => {
        this.opcoesProduto = produtos.listaProduto;
        this.opcoesProdutoFiltrado = produtos.listaProduto;
      }
    );
  }

  private _filter(value: string): ClienteComboModel[] {
    console.log("Cliente Mudou"+value);
    const filterValue = value.toString();
    return this.opcoesCliente.filter(
      item =>  {
        if(item.cdCliente?.toString().includes(filterValue) || item.nome?.toLowerCase().includes(filterValue)){
          return item;
        }
      }
    );
  }
  private _filterProduto(valuePro: string): ProdutosComboModel[] {
    const filterProValue = valuePro.toString().toLowerCase();
    return this.opcoesProduto.filter(
      item =>  {
        if(item.cdProduto.toString().includes(filterProValue) || item.nome?.toLowerCase().includes(filterProValue) || item.cdBarras?.toString().includes(filterProValue)){
          return item;
        }
      }
    );
  }
  

  showNotification(from, align, cor, mensagem) {
    // cor:   '','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: 'notifications',
        message: mensagem

    }, {
        type: cor,
        timer: 50,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }


}
