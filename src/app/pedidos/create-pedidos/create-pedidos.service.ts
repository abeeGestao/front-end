import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ClienteResponseModel } from 'app/clientes/models/cliente-response.model';
import { Observable } from 'rxjs';
import { ProdutosResponseModel } from 'app/produtos/models/produto-response.model';
import { PedidoModel } from '../models/pedido.model';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
import { ConfigService } from 'app/services/config';

@Injectable({
  providedIn: 'root'
})
export class CreatePedidosService {
  url : string;
  constructor(private host: ConfigService, private http: HttpClient) { 
    this.url = this.host.getApiBaseURL();
  }
  getAllClientes(): Observable<ClienteResponseModel>{
    return this.http.get<ClienteResponseModel>(this.url + "getAllClientes");
  }
  getAllProdutos(): Observable<ProdutosResponseModel> {
    return this.http.get<ProdutosResponseModel>(this.url + 'getAllProdutos');
  }

  postPedidos(request: PedidoModel): Observable<RespostaServico>{
    return this.http.post<RespostaServico>(this.url + "cadastroPedido", request);
  }
}
