import { TestBed } from '@angular/core/testing';

import { CreatePedidosService } from './create-pedidos.service';

describe('CreatePedidosService', () => {
  let service: CreatePedidosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreatePedidosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
