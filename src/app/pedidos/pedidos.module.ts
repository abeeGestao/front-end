import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { PedidosInfoComponent } from './pedidos-info/pedidos-info.component';
import { PedidosComponent } from './pedidos.component';

@NgModule({
  declarations: [PedidosComponent, PedidosInfoComponent],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class PedidosModule { }
