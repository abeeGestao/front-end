export interface PedidoFiltroRequestModel{
    cdPedido: number;
    cdCliente: string;
    cpfCnpjCliente: string;
    status: string;
    email: string;
}