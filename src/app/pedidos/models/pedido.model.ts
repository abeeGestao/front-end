import { Timestamp } from "rxjs";
import { ProdutosComboModel } from "./produto-combo.model";

export interface PedidoModel{
    cdPedido: number;
    cdCliente:number;
    nomeCliente: string;
    descricaoStatus: string;
    dataPedido: Date;
    cdStatus: string;
    observacao: string;
    desconto: string;
    valorTotal: number;
    quantidadeParcela: number;
    valorPago: number;
    formaPagamento: string;
    listaProduto: [ProdutosComboModel];
}