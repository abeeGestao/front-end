import { PedidoModel } from "app/pedidos/models/pedido.model";

export interface PedidoResponseModel{
    quantidadePagina: number;
    listaPedido: PedidoModel[];    
}