import { Component, OnInit } from '@angular/core';
import { PedidoModel } from 'app/pedidos/models/pedido.model';
import { PedidoResponseModel } from 'app/pedidos/models/pedido-response.model';
import { PedidoFiltroRequestModel } from 'app/pedidos/models/pedido-filtro-request.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PedidosService } from 'app/pedidos/pedidos.service';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
declare var $: any;

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.css']
})
export class PedidosComponent implements OnInit {

  public paginaAtual = 1;
  public codigoAtual = 0;
    
    pedido = {} as PedidoModel;
    pedidosResponse: PedidoResponseModel;
    request = {} as PedidoFiltroRequestModel;
    public filtrosGroup: FormGroup;
    public listaStatus = [
      {chave:"A", valor: "Aberto"},
      {chave:"C", valor: "Confirmado"}
    ];

    constructor(public service: PedidosService, private formBuilder: FormBuilder) { }

    ngOnInit(): void {
      this.filtrosGroup = this.formBuilder.group({
        cdPedido: [null],
        cdCliente: [null],
        documento: [null],
        status: [null],
        email: [null],
        cpfCnpjCliente: [null],
        pagina: [1],
        quantidadePagina: [5]
      });
      this.getPedidos();
    }

    public getPedidos() {
      this.service.getPedidos(this.filtrosGroup.value).subscribe(
        (pedidos: PedidoResponseModel) => {
          this.pedidosResponse = pedidos;
        }
      );
    }
    public deletePedido() {
      this.service.deltePedido(this.codigoAtual).subscribe(
        (response: RespostaServico) => {
          if(response.codigoServico == 0){
            this.showNotification("top","center", "success", "Pedido removido com sucesso!");
            this.getPedidos();
          }else{
            this.showNotification("top","center", "warning", "Falha ao tentar apagar o pedido!");
          }
          
          
        } 
      );
    }

    public atualizarTelaPedido(){
      this.getPedidos();
    }

    mudarPagina(numeroPagina: number) {
      this.filtrosGroup.controls['pagina'].setValue(numeroPagina);
      this.getPedidos();
      this.paginaAtual = numeroPagina;
    }

    proximaPagina() {
      console.log('+1 pagina')
      this.filtrosGroup.controls['pagina'].setValue(this.paginaAtual + 1);
      this.getPedidos();
      this.paginaAtual = this.paginaAtual + 1;
    }

    paginaAnterior() {
      if (this.paginaAtual > 1) {
        this.filtrosGroup.controls['pagina'].setValue(this.paginaAtual - 1);
        this.getPedidos();
        this.paginaAtual = this.paginaAtual - 1;
      }
    }

    public getPagina(numeroPosicao: number) {
      if (this.paginaAtual === 1) {
        if (numeroPosicao === 1) {
          return 1;
        }
        if (numeroPosicao === 2) {
          return 2;
        }
        if (numeroPosicao === 3) {
          return 3;
        }
      } else {
        if (numeroPosicao === 1) {
          return this.paginaAtual - 1;
        }
        if (numeroPosicao === 2) {
          return this.paginaAtual;
        }
        if (numeroPosicao === 3) {
          return this.paginaAtual + 1;
        }
      }
    }

    //Notificação jquery abaixo:
  showNotification(from, align, cor, mensagem){
    //cor:   '','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: "notifications",
        message: mensagem

    },{
        type: cor,
        timer: 50,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }

}
