import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { PedidosInfoFiltroRequestModel } from './models/pedidos-info-filtro-request.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PedidosInfoService } from './pedidos-info.service';
import { PedidosInfoResponseModel } from './models/pedidos-info-response.model';
import { PedidosInfoModel } from './models/pedidos-info.model';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
declare var $: any;

@Component({
  selector: 'app-pedidos-info',
  templateUrl: './pedidos-info.component.html',
  styleUrls: ['./pedidos-info.component.css']
})
export class PedidosInfoComponent implements OnInit {

  public paginaAtual = 1;

  requestPedido: PedidosInfoFiltroRequestModel = new PedidosInfoFiltroRequestModel();

  pedidosResponse: PedidosInfoResponseModel;

  public filtroGetClienteGroup: FormGroup;
  public FormularioAtualizarGroup: FormGroup;

  constructor(public servico: PedidosInfoService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.paginaAtual = 1;
    this.FormularioAtualizarGroup = this.formBuilder.group({
      cdPedido: [null],
      codigo: [null],
      nome: [null],
      quantidade: [null],
      valor: [null],
      total: [null],
      observacao: [null],
      cliente: [null],
      pagamento: [null],
      data: [null],
      pagina: [1],
      quantidadePagina: [5]
    });
  }

  @Input() set cdPedidoParam(codigo) {
    this.requestPedido.cdPedido = codigo;
    this.getPedido();
    //console.log("Pedido " + codigo);
  }

  @Output() respostaAtualizarClienteEvent = new EventEmitter();
  
  getPedido() {
    this.servico.getItens(this.requestPedido).subscribe(
      (pedidos: PedidosInfoResponseModel) => {
        /*const pedido: PedidosInfoModel = pedidos.listaItens[0];
        this.FormularioAtualizarGroup.setValue(pedido);*/
        this.pedidosResponse = pedidos;
      }
    );
  }

  mudarPagina(numeroPagina: number) {
    //console.log('+1 pagina')
    this.FormularioAtualizarGroup.controls['pagina'].setValue(numeroPagina);
    this.paginaAtual = numeroPagina;
    this.getPedido();
  }

  proximaPagina() {
    //console.log('+1 pagina')
    this.FormularioAtualizarGroup.controls['pagina'].setValue(this.paginaAtual + 1);
    this.getPedido();
    this.paginaAtual = this.paginaAtual + 1;
  }

  paginaAnterior() {
    //console.log('-1 pagina')
    if (this.paginaAtual > 1) {
      this.FormularioAtualizarGroup.controls['pagina'].setValue(this.paginaAtual - 1);
      this.getPedido();
      this.paginaAtual = this.paginaAtual - 1;
    }
  }

  public getPagina(numeroPosicao: number) {
    if (this.paginaAtual === 1) {
      if (numeroPosicao === 1) {
        return 1;
      }
      if (numeroPosicao === 2) {
        return 2;
      }
      if (numeroPosicao === 3) {
        return 3;
      }
    } else {
      if (numeroPosicao === 1) {
        return this.paginaAtual - 1;
      }
      if (numeroPosicao === 2) {
        return this.paginaAtual;
      }
      if (numeroPosicao === 3) {
        return this.paginaAtual + 1;
      }
    }
  }

  public ConfirmarPedido(){
    this.servico.confirmarPedido(this.pedidosResponse.listaItens[0]).subscribe(
      (response: RespostaServico) => {
        if (response.codigoServico === 0) {
          this.showNotification('top', 'center', 'success', 'Pedido Confirmado com sucesso!');
          this.getPedido();
          this.respostaAtualizarClienteEvent.emit("0");
        } else {
          this.showNotification('top', 'center', 'warning', 'Falha ao tentar Confirmado o pedido!');
        }
      }
    );
  }

  showNotification(from, align, cor, mensagem) {
    // cor:   '','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: 'notifications',
        message: mensagem

    }, {
        type: cor,
        timer: 50,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }

}
