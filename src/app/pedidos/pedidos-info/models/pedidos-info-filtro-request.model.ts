export class PedidosInfoFiltroRequestModel {
    cdPedido: number;
    codigo: number;
    nome: string;
    quantidade: number;
    valor: number
    total: number;
    observacao: string;
    cliente: string;
    pagamento: string;
    data: Date;
    pagina: number;
    quantidadePagina: number;
}