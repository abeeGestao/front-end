import { PedidosInfoModel } from './pedidos-info.model';

export interface PedidosInfoResponseModel {
    quantidadePagina: number;
    listaItens: PedidosInfoModel[];
}