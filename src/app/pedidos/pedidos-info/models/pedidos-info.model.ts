import { ClienteModel } from "app/clientes/models/cliente.model";

export interface PedidosInfoModel {
    cdPedido: number;
    codigo: number;
    nome: string;
    quantidade: number;
    valor: number
    total: number;
    observacao: string;
    cliente: string;
    pagamento: string;
    data: Date;
    clienteObjet: ClienteModel;
    status: string;
}