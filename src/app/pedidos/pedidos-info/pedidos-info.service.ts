import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { PedidosInfoFiltroRequestModel } from './models/pedidos-info-filtro-request.model';
import { PedidosInfoResponseModel } from './models/pedidos-info-response.model';
import { PedidoModel } from '../models/pedido.model';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
import { PedidosInfoModel } from './models/pedidos-info.model';
import { ConfigService } from 'app/services/config';

@Injectable({
    providedIn: 'root'
})
export class PedidosInfoService {
    url : string;
    constructor(private host: ConfigService, private http: HttpClient) { 
      this.url = this.host.getApiBaseURL();
    }
    getItens(request: PedidosInfoFiltroRequestModel): Observable<PedidosInfoResponseModel> {
        return this.http.post<PedidosInfoResponseModel>(this.url + 'getItensPedido', request);
    }

    confirmarPedido(request: PedidosInfoModel): Observable<RespostaServico>{
        return this.http.put<RespostaServico>(this.url + "confirmarPedido", request);
      }

    
}
