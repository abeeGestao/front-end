import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { ClientesComponent } from 'app/clientes/clientes.component';
import { PedidosComponent } from 'app/pedidos/pedidos.component';
import { ProdutosComponent } from 'app/produtos/produtos.component';
import { CreateClientesComponent } from 'app/clientes/create-clientes/create-clientes.component';
import { UsuarioComponent } from 'app/usuario/usuario.component';
import { CreateProdutosComponent } from 'app/produtos/create-produtos/create-produtos.component';
import { CaixaComponent } from 'app/caixa/caixa.component';
import { CreatePedidosComponent } from 'app/pedidos/create-pedidos/create-pedidos.component';
import { CreateUsuariosComponent } from 'app/usuario/create-usuarios/create-usuarios.component';
import { AlterClientesComponent } from 'app/clientes/alter-clientes/alter-clientes.component';
import { EmpresaComponent } from 'app/empresa/empresa.component';
import { LoginComponent } from 'app/login/login.component';
import { GuardaRotasService } from 'app/guarda-rotas/guarda-rotas.service';
import { GuardaRotasAdminService } from 'app/guarda-rotas/guarda-rotas-admin.service';
import { DespesaComponent } from 'app/despesa/despesa.component';
import { CreateDespesaComponent } from 'app/despesa/create-despesa/create-despesa.component';


export const AdminLayoutRoutes: Routes = [
    // {
    //   path: '',
    //   children: [ {
    //     path: 'dashboard',
    //     component: DashboardComponent
    // }]}, {
    // path: '',
    // children: [ {
    //   path: 'userprofile',
    //   component: UserProfileComponent
    // }]
    // }, {
    //   path: '',
    //   children: [ {
    //     path: 'icons',
    //     component: IconsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'notifications',
    //         component: NotificationsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'maps',
    //         component: MapsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'typography',
    //         component: TypographyComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'upgrade',
    //         component: UpgradeComponent
    //     }]
    // }
    { path: 'dashboard',      component: DashboardComponent, canActivate: [GuardaRotasAdminService] },
    { path: 'user-profile',   component: UserProfileComponent, canActivate: [GuardaRotasService] },
    { path: 'table-list',     component: TableListComponent, canActivate: [GuardaRotasService] },
    { path: 'typography',     component: TypographyComponent, canActivate: [GuardaRotasService] },
    { path: 'icons',          component: IconsComponent, canActivate: [GuardaRotasService] },
    { path: 'maps',           component: MapsComponent, canActivate: [GuardaRotasService] },
    { path: 'notifications',  component: NotificationsComponent, canActivate: [GuardaRotasService] },
    { path: 'upgrade',        component: UpgradeComponent, canActivate: [GuardaRotasService] },
    { path: 'clientes',       component: ClientesComponent, canActivate: [GuardaRotasService] },
    { path: 'pedidos',        component: PedidosComponent, canActivate: [GuardaRotasService]},
    { path: 'produtos',       component: ProdutosComponent, canActivate: [GuardaRotasService] },
    { path: 'criar-clientes', component: CreateClientesComponent, canActivate: [GuardaRotasService] },
    { path: 'criar-produtos', component: CreateProdutosComponent, canActivate: [GuardaRotasService] },
    { path: 'criar-pedidos',  component: CreatePedidosComponent, canActivate: [GuardaRotasService] },
    { path: 'criar-usuarios', component: CreateUsuariosComponent, canActivate: [GuardaRotasAdminService] },
    { path: 'criar-despesa',  component: CreateDespesaComponent, canActivate: [GuardaRotasAdminService] },
    { path: 'alter-clientes', component: AlterClientesComponent, canActivate: [GuardaRotasService] },
    { path: 'usuario',        component: UsuarioComponent, canActivate: [GuardaRotasAdminService] },
    { path: 'empresa',        component: EmpresaComponent, canActivate: [GuardaRotasAdminService] },
    { path: 'despesa',        component: DespesaComponent, canActivate: [GuardaRotasAdminService] }
];
