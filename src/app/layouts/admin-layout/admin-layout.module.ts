import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatRippleModule, MatNativeDateModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { ClientesComponent } from 'app/clientes/clientes.component';
import { PedidosComponent } from 'app/pedidos/pedidos.component';
import { ProdutosComponent } from 'app/produtos/produtos.component';
import { CreateClientesComponent } from 'app/clientes/create-clientes/create-clientes.component';
import { CreateProdutosComponent } from 'app/produtos/create-produtos/create-produtos.component';
import { CreatePedidosComponent } from 'app/pedidos/create-pedidos/create-pedidos.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { CreateUsuariosComponent } from 'app/usuario/create-usuarios/create-usuarios.component';
import { AlterClientesComponent } from 'app/clientes/alter-clientes/alter-clientes.component';
import { AlterProdutosComponent } from 'app/produtos/alter-produtos/alter-produtos.component';
import { EmpresaComponent } from 'app/empresa/empresa.component';
import { ComprasComponent } from 'app/dashboard/compras/compras.component';
import { PedidosInfoComponent } from 'app/pedidos/pedidos-info/pedidos-info.component';
import { UsuarioComponent } from 'app/usuario/usuario.component';
import { AlterUsuariosComponent } from 'app/usuario/alter-usuarios/alter-usuarios.component';
import { GraficoComponent } from 'app/dashboard/grafico/grafico.component';
import { DespesaComponent } from 'app/despesa/despesa.component';
import { AlterDespesaComponent } from 'app/despesa/alter-despesa/alter-despesa.component';
import { CreateDespesaComponent } from 'app/despesa/create-despesa/create-despesa.component';
import { GraficoDespesaComponent } from 'app/dashboard/grafico-despesa/grafico-despesa.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatAutocompleteModule
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
    ClientesComponent,
    PedidosComponent,
    ProdutosComponent,
    UsuarioComponent,
    CreateClientesComponent,
    CreateProdutosComponent,
    CreatePedidosComponent,
    CreateUsuariosComponent,
    AlterClientesComponent,
    AlterProdutosComponent,
    AlterUsuariosComponent,
    EmpresaComponent,
    PedidosInfoComponent,
    ComprasComponent,
    GraficoComponent,
    DespesaComponent,
    AlterDespesaComponent,
    CreateDespesaComponent,
    GraficoDespesaComponent
  ]
})

export class AdminLayoutModule {}
