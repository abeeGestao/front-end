import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

@Injectable()
export class ConfigService {

    public config: any;
    constructor(private http: Http) {
    }

    loadJSON(filePath) {
        const json = this.loadTextFileAjaxSync(filePath, "application/json");
        return JSON.parse(json);
    }

    getApiBaseURL(): string{
        //return "http://127.0.0.1:8989/";
        return this.loadJSON("assets/config.json").hostService;
    }

    loadTextFileAjaxSync(filePath, mimeType) {
        const xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", filePath, false);
        if (mimeType != null) {
            if (xmlhttp.overrideMimeType) {
                xmlhttp.overrideMimeType(mimeType);
            }
        }
        xmlhttp.send();
        if (xmlhttp.status == 200) {
            return xmlhttp.responseText;
        }
        else {
            return null;
        }
    }
}