import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';
import {
  AgmCoreModule
} from '@agm/core';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { HttpClientModule } from '@angular/common/http';

import { CaixaComponent } from './caixa/caixa.component';
import { LoginComponent } from './login/login.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { AutenticaoService } from './login/autenticao.service';


import {NgxMaskModule} from 'ngx-mask';
import { NgxCurrencyModule } from "ngx-currency";
import { PedidosInfoComponent } from './pedidos/pedidos-info/pedidos-info.component';
import { CommonModule, APP_BASE_HREF, LocationStrategy, HashLocationStrategy} from '@angular/common';
import { ConfigService } from './services/config';
import { DespesaComponent } from './despesa/despesa.component';
import { AlterDespesaComponent } from './despesa/alter-despesa/alter-despesa.component';
import { CreateDespesaComponent } from './despesa/create-despesa/create-despesa.component';


@NgModule({
  imports: [
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    MatAutocompleteModule,
    AgmCoreModule.forRoot({
      apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
    }),
    NgxMaskModule.forRoot(),
    NgxCurrencyModule
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    CaixaComponent,
    LoginComponent
  ],
  providers: [ConfigService, AutenticaoService,{ provide: APP_BASE_HREF, useValue: '/' },
  { provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
