import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { EmpresaComponent } from 'app/empresa/empresa.component';

@NgModule({
  declarations: [EmpresaComponent],
  imports: [
    CommonModule,
    HttpClientModule
  ]
})

export class EmpresaModule { }
