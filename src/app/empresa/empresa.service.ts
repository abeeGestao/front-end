import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
import { Observable } from 'rxjs';
import { EmpresaModel } from './models/empresa.model';
import { CepModel } from 'app/clientes/models/cep.model';
import { ConfigService } from 'app/services/config';

@Injectable({
  providedIn: 'root'
})
export class EmpresaService {

  url : string;
  constructor(private host: ConfigService, private http: HttpClient) { 
    this.url = this.host.getApiBaseURL();
  }
  
  getEmpresa(): Observable<EmpresaModel>{
    return this.http.get<EmpresaModel>(this.url + "getEmpresa");
  }
  atualizarEmpresa(request: EmpresaModel): Observable<RespostaServico>{
    return this.http.put<RespostaServico>(this.url + "putEmpresa", request);
  }
  getEndereco(cep: string): Observable<CepModel>{
    return this.http.get<CepModel>("https://viacep.com.br/ws/"+cep+"/json/");
  }
}