import { Component, OnInit } from '@angular/core';
import { EmpresaService } from './empresa.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { EmpresaModel } from './models/empresa.model';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
import { CepModel } from 'app/clientes/models/cep.model';
import { HttpClient, HttpEventType } from '@angular/common/http';


declare var $: any;
@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {

  public FormularioAtualizarGroup: FormGroup;
  fileData: File = null;
  previewUrl:any = null;
  fileUploadProgress: string = null;
  uploadedFilePath: string = "./assets/img/faces/marc.jpg";
  

  constructor( public empresaServico: EmpresaService, private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit(): void {
    this.FormularioAtualizarGroup = this.formBuilder.group({
      cdEmpresa: [null],
      nome: [null],
      documento: [null],
      razaoSocial: [null],
      nomeFantasia: [null],
      status: ["A"],
      email: [null],
      cep: [null],
      cidade: [null],
      estado: [null],
      pais: [null],
      bairro: [null],
      rua: [null],
      numero: [null],
      telefone: [null]
    });
    this.getEmpresa();
  } 

  getEmpresa() {    
    this.empresaServico.getEmpresa().subscribe(
      (empresa: EmpresaModel) => this.FormularioAtualizarGroup.setValue(empresa));
  }

  public atualizarEmpresa() {
    this.empresaServico.atualizarEmpresa(this.FormularioAtualizarGroup.value).subscribe(
      (response: RespostaServico) => {
        if (response.codigoServico === 0) {
          this.showNotification('top', 'center', 'success', 'Dados atualizados com sucesso!');
        } else {
          this.showNotification('top', 'center', 'warning', 'Falha ao tentar atualizar os dados!');
        }
      }
    );
  }

  public buscarPorCep(cep: any){
    this.empresaServico.getEndereco(cep).subscribe(
      (endereco: CepModel) => {
        this.FormularioAtualizarGroup.patchValue({
          cidade: endereco.localidade, 
          estado: endereco.uf,
          rua: endereco.logradouro,
          bairro: endereco.bairro
        });
      }
    )
  }
 

  // Modal de Notificação jquery abaixo:
  showNotification(from, align, cor, mensagem) {
    // cor:   '','info','success','warning','danger'];
    const color = Math.floor((Math.random() * 4) + 1);
    $.notify({
        icon: 'notifications',
        message: mensagem
    }, {
        type: cor,
        timer: 50,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }

  //Upload de imagem
 
  fileProgress(fileInput: any) {
    
    
    this.fileData = <File>fileInput.target.files[0];
    this.preview();
  }
 
  preview() {
      // Show preview 
      var mimeType = this.fileData.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
  
      var reader = new FileReader();      
      reader.readAsDataURL(this.fileData); 
      reader.onload = (_event) => { 
        this.previewUrl = reader.result; 
      }
  }

  uploadImage(){
    $(".abee-inputNone").click();
  }
}
