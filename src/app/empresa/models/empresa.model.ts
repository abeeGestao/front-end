export interface EmpresaModel{
    cdEmpresa: number ;
    nome: String ;
    documento: String ;
    razaoSocial: String ;
    nomeFantasia: String ;
    status: String ;
    email: String ;
    telefone: String ;
    cep: String ;
    rua: String ;
    numero: String ;
    cidade: String ;
    estado: String ;
    pais: String ;
    bairro: String;
}