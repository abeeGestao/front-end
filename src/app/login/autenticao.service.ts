import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioModel } from 'app/usuario/models/usuario.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AutenticacaoModel } from './model/autenticacao-model';
import { UsuarioAuthModel } from './model/usuario-auth-model';
import { of } from 'rxjs';
import 'rxjs/add/operator/map';
import { ConfigService } from 'app/services/config';

@Injectable({
  providedIn: 'root'
})
export class AutenticaoService {
  
  private usuarioAutenticado: boolean = false;
  private auth: AutenticacaoModel = new AutenticacaoModel();
  url : string;
 
  constructor(private host: ConfigService,private router: Router, private http: HttpClient) {
    this.url = this.host.getApiBaseURL();
    if(sessionStorage.getItem("nome") != null && sessionStorage.getItem("nome") != ""){
      this.auth.usuario = sessionStorage.getItem("nome");
      this.auth.perfil = Number(sessionStorage.getItem("perfil"));
      this.auth.nome = sessionStorage.getItem("nome");
      this.auth.autenticado = true;
      this.usuarioAutenticado = true;
      console.log("Logado ");
    }else{
      console.log("Não logado");
    }
   }


  

  postAutenticacao(request: UsuarioAuthModel): Observable<AutenticacaoModel>{
    return this.http.post<AutenticacaoModel>(this.url + "autenticacao", request);
  }

  public autenticar( usuarioR, senhaR ): Observable<boolean>{
    let user: UsuarioAuthModel = new UsuarioAuthModel();
    user.nomeUsuario = usuarioR;
    user.senha= senhaR;
    return this.postAutenticacao(user).map( a => {
      this.auth = a;
      sessionStorage.setItem("nomeUsuario",a.usuario);
      sessionStorage.setItem("perfil",a.perfil.toString());
      sessionStorage.setItem("nome", a.nome);
      return this.fazerLogin();
    });
  }
  public isAutenticado(): boolean{
    return this.usuarioAutenticado;
  }
  public isAdmin(): boolean{
    if(this.auth.perfil == 1 && this.auth.autenticado){
      return true;
    }else{
      return false;
    }
  }

  public fazerLogin(): boolean{
    if(this.auth.autenticado){
      this.usuarioAutenticado = true;
      this.router.navigate(['/']);
      return true;
    }else{
      this.usuarioAutenticado = false;
      return false;
    }
  }

  public deslogar(){
    sessionStorage.clear();
    this.auth.autenticado = true;
    this.usuarioAutenticado = true;
    this.router.navigate(['/login']);
    /*    sessionStorage.setItem("nome", null);
    sessionStorage.setItem("perfil",null);
    sessionStorage.setItem("nome", null);*/ 
  }

  public getPerfil(): number{
    return this.auth.perfil;
  }

  public getNomeUsuario(): string {
    return this.auth.nome;
  }

}
