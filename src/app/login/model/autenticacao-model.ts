export class AutenticacaoModel{
    usuario: string;
    nome: string;
    autenticado: boolean;
    perfil: number;
}