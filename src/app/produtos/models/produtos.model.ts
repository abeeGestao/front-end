export interface ProdutosModel {
    cdProduto: number;
    nome: string;
    cdBarras: string;
    quantidade: number;
    valor: number;
    descricao: string;
    status: string;
}
