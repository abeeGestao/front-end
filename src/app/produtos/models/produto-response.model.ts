import { ProdutosModel } from './produtos.model';

export interface ProdutosResponseModel {
    quantidadePagina: number;
    listaProduto: ProdutosModel[];
}
