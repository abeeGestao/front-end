export class ProdutosFiltroRequestModel {
    cdProduto: number;
    nome: string;
    codBarras: string;
    quantidade: number;
    valor: number;
    descricao: string;
    status: string;
    pagina: number;
    quantidadePagina: number;
}
