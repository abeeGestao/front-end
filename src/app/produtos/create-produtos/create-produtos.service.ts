import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
import { Observable } from 'rxjs';
import { ProdutosModel } from '../models/produtos.model';
import { ConfigService } from 'app/services/config';

@Injectable({
  providedIn: 'root'
})
export class CreateProdutosService {

  url : string;
  constructor(private host: ConfigService, private http: HttpClient) { 
    this.url = this.host.getApiBaseURL();
  }

  postProdutos(request: ProdutosModel): Observable<RespostaServico>{
    return this.http.post<RespostaServico>(this.url + "cadastroProduto", request);
  }

}
