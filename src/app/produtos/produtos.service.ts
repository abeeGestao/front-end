import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ProdutosFiltroRequestModel } from './models/produtos-filtro-request.model';
import { Observable } from 'rxjs';
import { ProdutosResponseModel } from './models/produto-response.model';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
import { ProdutosModel } from './models/produtos.model';
import { ConfigService } from 'app/services/config';

@Injectable({
    providedIn: 'root'
})
export class ProdutosService {
    url : string;
    constructor(private host: ConfigService, private http: HttpClient) { 
      this.url = this.host.getApiBaseURL();
    }
    getProdutos(request: ProdutosFiltroRequestModel): Observable<ProdutosResponseModel> {
        return this.http.post<ProdutosResponseModel>(this.url + 'getProdutos', request);
    }

    deleteProduto(id: number): Observable<RespostaServico> {
        console.log('Requisição Delete, ID:' + id)
        return this.http.delete<RespostaServico>(this.url + 'deleteProduto/' + id);
    }
}
