import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProdutosComponent } from 'app/produtos/produtos.component';
import { CreateProdutosComponent } from './create-produtos/create-produtos.component';
import { AlterProdutosComponent } from './alter-produtos/alter-produtos.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ProdutosComponent, CreateProdutosComponent, AlterProdutosComponent],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class ProdutosModule { }
