import { Component, OnInit } from '@angular/core';
import { ProdutosModel } from './models/produtos.model';
import { ProdutosFiltroRequestModel } from './models/produtos-filtro-request.model';
import { ProdutosService } from './produtos.service';
import { ProdutosResponseModel } from './models/produto-response.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
declare var $: any;

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.css']
})
export class ProdutosComponent implements OnInit {

  public paginaAtual = 1;
  public codigoAtual = 0;

  produto = {} as ProdutosModel;

  produtosResponse: ProdutosResponseModel;
  request = {} as ProdutosFiltroRequestModel;
  public filtrosGroup: FormGroup;

  constructor(public service: ProdutosService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.paginaAtual = 1;
    this.filtrosGroup = this.formBuilder.group({
      cdProduto: [null],
      cdBarras: [null],
      nome: [null],
      descricao: [null],
      quantidade: [null],
      status: [null],
      valor: [null],
      pagina: [1],
      quantidadePagina: [5]
    });
    this.getProdutos();
  }

  getProdutos() {
    this.service.getProdutos(this.filtrosGroup.value).subscribe(
      (produtos: ProdutosResponseModel) => {
        this.produtosResponse = produtos;
      }
    );
  }

  deleteProduto() {
    this.service.deleteProduto(this.codigoAtual).subscribe(
      (response: RespostaServico) => {
        if (response.codigoServico === 0) {
          this.showNotification('top', 'center', 'success', 'Produto removido com sucesso!');
          this.getProdutos();
        } else {
          this.showNotification('top', 'center', 'warning', 'Falha ao tentar apagar o Produto!');
        }
      }
    );
  }


  reciverFeedback(response: string) {
      console.log(response);
      this.getProdutos();
  }

  mudarPagina(numeroPagina: number) {
    this.filtrosGroup.controls['pagina'].setValue(numeroPagina);
    this.getProdutos();
    this.paginaAtual = numeroPagina;
  }

  proximaPagina() {
    console.log('+1 pagina')
    this.filtrosGroup.controls['pagina'].setValue(this.paginaAtual + 1);
    this.getProdutos();
    this.paginaAtual = this.paginaAtual + 1;
  }

  paginaAnterior() {
    if (this.paginaAtual > 1) {
      this.filtrosGroup.controls['pagina'].setValue(this.paginaAtual - 1);
      this.getProdutos();
      this.paginaAtual = this.paginaAtual - 1;
    }
  }

  public getPagina(numeroPosicao: number) {
    if (this.paginaAtual === 1) {
      if (numeroPosicao === 1) {
        return 1;
      }
      if (numeroPosicao === 2) {
        return 2;
      }
      if (numeroPosicao === 3) {
        return 3;
      }
    } else {
      if (numeroPosicao === 1) {
        return this.paginaAtual - 1;
      }
      if (numeroPosicao === 2) {
        return this.paginaAtual;
      }
      if (numeroPosicao === 3) {
        return this.paginaAtual + 1;
      }
    }
  }

  // Notificação jquery abaixo:
  showNotification(from, align, cor, mensagem) {
    // cor:   '','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: 'notifications',
        message: mensagem

    }, {
        type: cor,
        timer: 50,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }

}
