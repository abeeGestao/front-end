import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
import { ProdutosModel } from '../models/produtos.model';
import { ProdutosResponseModel } from '../models/produto-response.model';
import { ProdutosFiltroRequestModel } from '../models/produtos-filtro-request.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AlterProdutosService } from './alter-produtos.service';
import { ProdutosService } from '../produtos.service';
import { CreateProdutosService } from '../create-produtos/create-produtos.service';
declare var $: any;

@Component({
  selector: 'app-alter-produtos',
  templateUrl: './alter-produtos.component.html',
  styleUrls: ['./alter-produtos.component.css']
})
export class AlterProdutosComponent implements OnInit {

  requestProduto: ProdutosFiltroRequestModel = new ProdutosFiltroRequestModel();
  public filtroGetClienteGroup: FormGroup;
  public FormularioAtualizarGroup: FormGroup;

  @Output() respostaAtualizacao = new EventEmitter();

  constructor(public service: CreateProdutosService,
    public servicoConsulta: ProdutosService,
    public servicoUpdateProduto: AlterProdutosService,
    private formBuilder: FormBuilder
    ) { }

  ngOnInit(): void {
    //abaixo setar formulário de requisição
    this.requestProduto.quantidadePagina = 5;
    this.requestProduto.pagina = 1;
    //Abaixo formulario para atualização do cliente
    this.FormularioAtualizarGroup = this.formBuilder.group({
      cdProduto: [null],
      cdBarras: [null],
      nome: [null],
      descricao: [null],
      quantidade: [null],
      status: ['A'],
      valor: [null]
    });
  }

  @Input() set cdProdutoParam(codigo){
    this.requestProduto.cdProduto = codigo;
    this.getProduto();
  }

  getProduto() {
    this.servicoConsulta.getProdutos(this.requestProduto).subscribe(
      (produtos: ProdutosResponseModel) => {
        const produto: ProdutosModel = produtos.listaProduto[0];
        this.FormularioAtualizarGroup.setValue(produto);
      }
    );
  }

  public atualizarProduto() {
    this.servicoUpdateProduto.atualizaProduto(this.FormularioAtualizarGroup.value).subscribe(
      (response: RespostaServico) => {
        if (response.codigoServico === 0) {
          this.showNotification('top', 'center', 'success', 'Produto alterado com sucesso!');
          this.feedback();
        } else {
          this.showNotification('top', 'center', 'warning', 'Falha ao tentar alterar o produto!');
        }
      }
    );
  }

  feedback() {
    this.respostaAtualizacao.emit({'resposta':'OK'});
  }

  showNotification(from, align, cor, mensagem) {
    // cor:   '','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: 'notifications',
        message: mensagem

    }, {
        type: cor,
        timer: 50,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }

}
