import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { ProdutosModel } from '../models/produtos.model';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
import { ConfigService } from 'app/services/config';

@Injectable({
    providedIn: 'root'
})
export class AlterProdutosService {
    url : string;
    constructor(private host: ConfigService, private http: HttpClient) { 
      this.url = this.host.getApiBaseURL();
    }

    atualizaProduto(request: ProdutosModel): Observable<RespostaServico>{
        return this.http.post<RespostaServico>(this.url + "atualizaProduto", request);
    }
}
