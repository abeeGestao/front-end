import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlterProdutosComponent } from './alter-produtos.component';

describe('AlterProdutosComponent', () => {
  let component: AlterProdutosComponent;
  let fixture: ComponentFixture<AlterProdutosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlterProdutosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlterProdutosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
