import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AutenticaoService } from 'app/login/autenticao.service';

@Injectable({
  providedIn: 'root'
})
export class GuardaRotasService implements CanActivate{

  constructor(private servicoAutenticacao: AutenticaoService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot): boolean | Observable<boolean>  {
    
      if(this.servicoAutenticacao.isAutenticado()){
        return true;
      }else{
        this.router.navigate(['/login']);
        return false;
      }
  }


}
