import { Injectable } from '@angular/core';
import { AutenticaoService } from 'app/login/autenticao.service';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GuardaRotasAdminService implements CanActivate{

  constructor(private servicoAutenticacao: AutenticaoService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean | Observable<boolean>  {
      
        if(this.servicoAutenticacao.isAdmin()){
          return true;
        }else{
          if(this.servicoAutenticacao.isAutenticado()){
            this.router.navigate(['/pedidos']);
            return true;
          }
          this.router.navigate(['/login']);
          return false;
        }
    }
}
