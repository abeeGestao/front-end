import { TestBed } from '@angular/core/testing';

import { GuardaRotasAdminService } from './guarda-rotas-admin.service';

describe('GuardaRotasAdminService', () => {
  let service: GuardaRotasAdminService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GuardaRotasAdminService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
