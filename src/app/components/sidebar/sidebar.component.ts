import { Component, OnInit } from '@angular/core';
import { GuardaRotasService } from 'app/guarda-rotas/guarda-rotas.service';
import { AutenticaoService } from 'app/login/autenticao.service';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/clientes', title: 'Gestão de Clientes',  icon: 'person', class: '' },
    { path: '/pedidos', title: 'Gestão dos Pedidos',  icon: 'assignment', class: '' },
    { path: '/user-profile', title: 'User Profile',  icon:'person', class: '' },
    { path: '/produto', title: 'Gestão de Produtos',  icon: 'local_offer', class: '' },
    //{ path: '/table-list', title: 'Table List',  icon: 'content_paste', class: '' },
    { path: '/caixa', title: 'Caixa',  icon: 'add_shopping_cart', class: '' },
    // { path: '/typography', title: 'Typography',  icon:'library_books', class: '' },
    // { path: '/icons', title: 'Icons',  icon:'bubble_chart', class: '' },
    // { path: '/maps', title: 'Maps',  icon:'location_on', class: '' },
    // { path: '/notifications', title: 'Notifications',  icon:'notifications', class: '' },
    // { path: '/upgrade', title: 'Upgrade to PRO',  icon:'unarchive', class: 'active-pro' },
];

export const ROUTES_ADMIN: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard',  icon: 'dashboard', class: '' },
  { path: '/clientes', title: 'Gestão de Clientes',  icon: 'person', class: '' },
  { path: '/pedidos', title: 'Gestão dos Pedidos',  icon: 'assignment', class: '' },
  //{ path: '/user-profile', title: 'User Profile',  icon:'person', class: '' },
  { path: '/produtos', title: 'Gestão de Produtos',  icon: 'local_offer', class: '' },
  { path: '/despesa', title: 'Gestão de Despesas',  icon: 'local_atm', class: '' },
  //{ path: '/table-list', title: 'Table List',  icon: 'content_paste', class: '' },
  { path: '/usuario', title: 'Gestão de Usuários',  icon: 'important_devices', class: '' },
  { path: '/caixa', title: 'Caixa',  icon: 'add_shopping_cart', class: '' },
  { path: '/empresa', title: 'Dados da Empresa',  icon: 'store', class: '' },
  // { path: '/typography', title: 'Typography',  icon:'library_books', class: '' },
  // { path: '/icons', title: 'Icons',  icon:'bubble_chart', class: '' },
  // { path: '/maps', title: 'Maps',  icon:'location_on', class: '' },
  // { path: '/notifications', title: 'Notifications',  icon:'notifications', class: '' },
  // { path: '/upgrade', title: 'Upgrade to PRO',  icon:'unarchive', class: 'active-pro' },
];

export const TODAS_ROTAS: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard',  icon: 'dashboard', class: '' },
  { path: '/clientes', title: 'Gestão de Clientes',  icon: 'person', class: '' },
  { path: '/criar-clientes', title: 'Gestão de Clientes',  icon: 'person', class: '' },
  { path: '/pedidos', title: 'Gestão dos Pedidos',  icon: 'assignment', class: '' },
  { path: '/criar-pedidos', title: 'Gestão dos Pedidos',  icon: 'assignment', class: '' },
  { path: '/user-profile', title: 'User Profile',  icon:'person', class: '' },
  { path: '/produtos', title: 'Gestão de Produtos',  icon: 'local_offer', class: '' },
  { path: '/criar-produtos', title: 'Gestão de Produtos',  icon: 'local_offer', class: '' },
  { path: '/despesa', title: 'Gestão de Despesas',  icon: 'local_atm', class: '' },
  //{ path: '/table-list', title: 'Table List',  icon: 'content_paste', class: '' },
  { path: '/usuario', title: 'Gestão de Usuários',  icon: 'important_devices', class: '' },
  { path: '/criar-usuarios', title: 'Gestão de Usuários',  icon: 'important_devices', class: '' },
  { path: '/caixa', title: 'Caixa',  icon: 'add_shopping_cart', class: '' },
  { path: '/empresa', title: 'Dados da Empresa',  icon: 'store', class: '' },
  // { path: '/typography', title: 'Typography',  icon:'library_books', class: '' },
  // { path: '/icons', title: 'Icons',  icon:'bubble_chart', class: '' },
  // { path: '/maps', title: 'Maps',  icon:'location_on', class: '' },
  // { path: '/notifications', title: 'Notifications',  icon:'notifications', class: '' },
  // { path: '/upgrade', title: 'Upgrade to PRO',  icon:'unarchive', class: 'active-pro' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(private auth: AutenticaoService) { }

  ngOnInit() {
    if(this.auth.getPerfil() == 1){
      this.menuItems = ROUTES_ADMIN.filter(menuItem => menuItem);
    }else{
      this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
    
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
  public deslogar(){
    this.auth.deslogar();
  }
}
