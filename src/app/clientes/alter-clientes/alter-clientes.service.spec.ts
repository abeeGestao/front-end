import { TestBed } from '@angular/core/testing';

import { AlterClientesService } from './alter-clientes.service';

describe('AlterClientesService', () => {
  let service: AlterClientesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AlterClientesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
