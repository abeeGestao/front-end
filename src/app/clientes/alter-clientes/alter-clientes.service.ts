import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ClienteModel } from '../models/cliente.model';
import { RespostaServico } from '../models/resposta-servico.model';
import { Observable } from 'rxjs';
import { ConfigService } from 'app/services/config';

@Injectable({
  providedIn: 'root'
})
export class AlterClientesService {

  url : string;
  constructor(private host: ConfigService, private http: HttpClient) { 
    this.url = this.host.getApiBaseURL();
  }
  
  atualizarCliente(request: ClienteModel): Observable<RespostaServico>{
    return this.http.put<RespostaServico>(this.url + "atualizarCliente", request);
  }
}
