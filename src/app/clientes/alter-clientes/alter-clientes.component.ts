import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CreateClientesService } from '../create-clientes/create-clientes.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RespostaServico } from '../models/resposta-servico.model';
import { CepModel } from '../models/cep.model';
import { ClientesService } from '../clientes.service';
import { ClienteResponseModel } from '../models/cliente-response.model';
import { ClienteModel } from '../models/cliente.model';
import { AlterClientesService } from './alter-clientes.service';
import { ClienteFiltroRequestModel } from '../models/cliente-filtro-request.model';

declare var $: any;

@Component({
  selector: 'app-alter-clientes',
  templateUrl: './alter-clientes.component.html',
  styleUrls: ['./alter-clientes.component.css']
})
export class AlterClientesComponent implements OnInit {

  requestCliente: ClienteFiltroRequestModel = new ClienteFiltroRequestModel();
  public filtroGetClienteGroup: FormGroup;
  public FormularioAtualizarGroup: FormGroup;
  @Input() atualizarTelaCliente: Function;

  @Output() respostaAtualizarClienteEvent = new EventEmitter();

  constructor(public service: CreateClientesService, 
    public servicoConsulta: ClientesService, 
    public servicoUpdateCliente: AlterClientesService,
    private formBuilder: FormBuilder
    ) { }

  ngOnInit(): void {
    //abaixo setar formulário de requisição
    this.requestCliente.quantidadePagina = 5;
    this.requestCliente.pagina = 1;
    //Abaixo formulario para atualização do cliente
    this.FormularioAtualizarGroup = this.formBuilder.group({
      cdCliente: [null],
      nome: [null],
      documento: [null],
      razaoSocial: [null],
      nomeFantasia: [null],
      status: ["A"],
      email: [null],
      cep: [null],
      cidade: [null],
      estado: [null],
      pais: [null],
      bairro: [null],
      rua: [null],
      numero: [null],
      telefone: [null]
    });
  }
  
  @Input() set cdClienteParam(codigo){
    this.requestCliente.cdCliente = codigo;
    this.getCliente();
  }

  getCliente() {    
    this.servicoConsulta.getClientes(this.requestCliente).subscribe(
      (clientes: ClienteResponseModel) => {
        const cliente: ClienteModel = clientes.listaCliente[0];
        this.FormularioAtualizarGroup.setValue(cliente);
      }
    );
  }

  public atualizarCliente() {
    this.servicoUpdateCliente.atualizarCliente(this.FormularioAtualizarGroup.value).subscribe(
      (response: RespostaServico) => {
        if (response.codigoServico === 0) {
          this.showNotification('top', 'center', 'success', 'Cliente cadastrado com sucesso!');
          this.respostaAtualizarClienteEvent.emit("0");
        } else {
          this.showNotification('top', 'center', 'warning', 'Falha ao tentar cadastrar o cliente!');
        }
      }
    );
  }

  public buscarPorCep(cep: any){
    this.service.getEndereco(cep).subscribe(
      (endereco: CepModel) => {
        this.FormularioAtualizarGroup.patchValue({
          cidade: endereco.localidade, 
          estado: endereco.uf,
          rua: endereco.logradouro,
          bairro: endereco.bairro
        });
      }
    )
  }
  // Modal de Notificação jquery abaixo:
  showNotification(from, align, cor, mensagem) {
    // cor:   '','info','success','warning','danger'];
    const color = Math.floor((Math.random() * 4) + 1);
    $.notify({
        icon: 'notifications',
        message: mensagem
    }, {
        type: cor,
        timer: 50,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }
}
