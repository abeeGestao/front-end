import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlterClientesComponent } from './alter-clientes.component';

describe('AlterClientesComponent', () => {
  let component: AlterClientesComponent;
  let fixture: ComponentFixture<AlterClientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlterClientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlterClientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
