import { Component, OnInit, EventEmitter } from '@angular/core';
import { ClientesService } from 'app/clientes/clientes.service';
import { ClienteModel } from 'app/clientes/models/cliente.model';
import { ClienteFiltroRequestModel } from 'app/clientes/models/cliente-filtro-request.model';
import { ClienteResponseModel } from 'app/clientes/models/cliente-response.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RespostaServico } from './models/resposta-servico.model';

declare var $: any;

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  public paginaAtual = 1;
  public codigoAtual = 0;
  cliente = {} as ClienteModel;
  clientesResponse: ClienteResponseModel;
  request = {} as ClienteFiltroRequestModel;
  public filtrosGroup: FormGroup;
  public mudouClienteEvent = new EventEmitter();
  public rotaNovoCliente =  { path: '/criar-clientes', title: 'Gestão de Clientes',  icon: 'person', class: '' };

  constructor(public service: ClientesService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.paginaAtual = 1;
    this.filtrosGroup = this.formBuilder.group({
      cdCliente: [null],
      nome: [null],
      documento: [null],
      razaoSocial: [null],
      nomeFantasia: [null],
      status: [null],
      email: [null],
      pagina: [1],
      quantidadePagina: [5]
    });
    this.getClientes();
    
  }

  getClientes() {
    this.service.getClientes(this.filtrosGroup.value).subscribe(
      (clientes: ClienteResponseModel) => {
        this.clientesResponse = clientes;
      }
    );
  }

  atualizarTelaCliente(evento){
    this.getClientes();
  }

  
  deleteCliente() {
    this.service.delteCliente(this.codigoAtual).subscribe(
      (response: RespostaServico) => {
        if (response.codigoServico === 0) {
          this.getClientes();
          this.showNotification('top', 'center', 'success', 'Cliente removido com sucesso!');
        } else {
          this.showNotification('top', 'center', 'warning', 'Falha ao tentar apagar o cliente!');
        }
      }
    );
  }

  mudarPagina(numeroPagina: number) {
    this.filtrosGroup.controls['pagina'].setValue(numeroPagina);
    this.getClientes();
    this.paginaAtual = numeroPagina;
  }

  proximaPagina() {
    console.log('+1 pagina')
    this.filtrosGroup.controls['pagina'].setValue(this.paginaAtual + 1);
    this.getClientes();
    this.paginaAtual = this.paginaAtual + 1;
  }

  paginaAnterior() {
    if (this.paginaAtual > 1) {
      this.filtrosGroup.controls['pagina'].setValue(this.paginaAtual - 1);
      this.getClientes();
      this.paginaAtual = this.paginaAtual - 1;
    }
  }

  public getPagina(numeroPosicao: number) {
    if (this.paginaAtual === 1) {
      if (numeroPosicao === 1) {
        return 1;
      }
      if (numeroPosicao === 2) {
        return 2;
      }
      if (numeroPosicao === 3) {
        return 3;
      }
    } else {
      if (numeroPosicao === 1) {
        return this.paginaAtual - 1;
      }
      if (numeroPosicao === 2) {
        return this.paginaAtual;
      }
      if (numeroPosicao === 3) {
        return this.paginaAtual + 1;
      }
    }
  }

  // Notificação jquery abaixo:
  showNotification(from, align, cor, mensagem) {
    // cor:   '','info','success','warning','danger'];
    const color = Math.floor((Math.random() * 4) + 1);
    $.notify({
        icon: 'notifications',
        message: mensagem
    }, {
        type: cor,
        timer: 50,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }
}
