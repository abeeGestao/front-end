import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientesComponent } from 'app/clientes/clientes.component';
import { CreateClientesComponent } from './create-clientes/create-clientes.component';
import { AlterClientesComponent } from './alter-clientes/alter-clientes.component';




@NgModule({
  declarations: [ClientesComponent, CreateClientesComponent, AlterClientesComponent],
  imports: [
    CommonModule,
  ]
})
export class ClientesModule { }
