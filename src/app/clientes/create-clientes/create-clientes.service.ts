import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ClienteModel } from '../models/cliente.model';
import { RespostaServico } from '../models/resposta-servico.model';
import { Observable } from 'rxjs';
import { CepModel } from '../models/cep.model';
import { ConfigService } from 'app/services/config';

@Injectable({
  providedIn: 'root'
})
export class CreateClientesService {

  url : string;
  constructor(private host: ConfigService, private http: HttpClient) { 
    this.url = this.host.getApiBaseURL();
  }
  
  postClientes(request: ClienteModel): Observable<RespostaServico>{
    return this.http.post<RespostaServico>(this.url + "cadastroCliente", request);
  }

  getEndereco(cep: string): Observable<CepModel>{
    return this.http.get<CepModel>("https://viacep.com.br/ws/"+cep+"/json/");
  }
  
}
