import { Component, OnInit } from '@angular/core';
import { CreateClientesService } from './create-clientes.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ClienteModel } from '../models/cliente.model';
import { RespostaServico } from '../models/resposta-servico.model';
import { CepModel } from '../models/cep.model';
declare var $: any;

@Component({
  selector: 'app-create-clientes',
  templateUrl: './create-clientes.component.html',
  styleUrls: ['./create-clientes.component.css']
})
export class CreateClientesComponent implements OnInit {
  public filtrosGroup: FormGroup;

  constructor(public service: CreateClientesService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.filtrosGroup = this.formBuilder.group({
      nome: [null],
      documento: [null],
      razaoSocial: [null],
      nomeFantasia: [null],
      status: ["A"],
      email: [null],
      cep: [null],
      cidade: [null],
      estado: [null],
      pais: [null],
      bairro: [null],
      rua: [null],
      numero: [null],
      telefone: [null]
    });
  }
  
  public postClientes() {
    this.service.postClientes(this.filtrosGroup.value).subscribe(
      (response: RespostaServico) => {
        if (response.codigoServico === 0) {
          this.showNotification('top', 'center', 'success', 'Cliente cadastrado com sucesso!');
        } else {
          this.showNotification('top', 'center', 'warning', 'Falha ao tentar cadastrar o cliente!');
        }
      }
    );
  }

  public buscarPorCep(cep: any){
    this.service.getEndereco(cep).subscribe(
      (endereco: CepModel) => {
        this.filtrosGroup.patchValue({
          cidade: endereco.localidade, 
          estado: endereco.uf,
          rua: endereco.logradouro,
          bairro: endereco.bairro
        });
      }
    )
    
    console.log("Cep:"+cep );
  }
  // Notificação jquery abaixo:
  showNotification(from, align, cor, mensagem) {
    // cor:   '','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: 'notifications',
        message: mensagem

    }, {
        type: cor,
        timer: 50,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }

}
