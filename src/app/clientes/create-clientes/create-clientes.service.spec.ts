import { TestBed } from '@angular/core/testing';

import { CreateClientesService } from './create-clientes.service';

describe('CreateClientesService', () => {
  let service: CreateClientesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreateClientesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
