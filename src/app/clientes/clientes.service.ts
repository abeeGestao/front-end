import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ClienteFiltroRequestModel } from 'app/clientes/models/cliente-filtro-request.model';
import { Observable } from 'rxjs';
import { ClienteResponseModel } from 'app/clientes/models/cliente-response.model';
import { RespostaServico } from './models/resposta-servico.model';
import { CepModel } from './models/cep.model';
import { ConfigService } from 'app/services/config';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {
  url : string;
  constructor(private host: ConfigService, private http: HttpClient) { 
    this.url = this.host.getApiBaseURL();
  }
  getClientes(request: ClienteFiltroRequestModel): Observable<ClienteResponseModel>{
    return this.http.post<ClienteResponseModel>(this.url + "getClientes", request);
  }
  delteCliente(id: number): Observable<RespostaServico>{
    console.log("Requisição Delete, ID:"+id)
    return this.http.delete<RespostaServico>(this.url + "deleteCliente/"+id);
  }

  getEndereco(cep: string): Observable<CepModel>{
    return this.http.get<CepModel>("https://viacep.com.br/ws/"+cep+"/json/");
  }
}
