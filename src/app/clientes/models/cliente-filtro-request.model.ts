export class ClienteFiltroRequestModel{
    cdCliente: number ; 
    nome: String ;
    documento: String ;
    razaoSocial: String ;
    nomeFantasia: String ;
    status: String ;
    email: String ;
    pagina: number;
    quantidadePagina: number;
}

