import { ClienteModel } from './cliente.model';

export interface ClienteResponseModel {
    quantidadePagina: number;
    listaCliente: ClienteModel[];
}