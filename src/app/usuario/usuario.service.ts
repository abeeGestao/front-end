import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UsuarioResponseModel } from './models/usuario-response.model';
import { UsuarioFiltroRequestModel } from './models/usuario-filtro-request.model';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
import { ConfigService } from 'app/services/config';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  url : string;
  constructor(private host: ConfigService, private http: HttpClient) { 
    this.url = this.host.getApiBaseURL();
  }
  getUsuarios(request: UsuarioFiltroRequestModel): Observable<UsuarioResponseModel>{
    return this.http.post<UsuarioResponseModel>(this.url + 'getUsuarios', request);
  }
  deleteUsuario(id: number): Observable<RespostaServico> {
    console.log('Requisição Delete, ID:' + id)
    return this.http.delete<RespostaServico>(this.url + 'deleteUsuario/' + id);
  }
}
