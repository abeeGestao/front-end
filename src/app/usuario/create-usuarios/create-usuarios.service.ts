import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
import { Observable } from 'rxjs';
import { UsuarioModel } from '../models/usuario.model';
import { ConfigService } from 'app/services/config';

@Injectable({
  providedIn: 'root'
})
export class CreateUsuariosService {

  url : string;
  constructor(private host: ConfigService, private http: HttpClient) { 
    this.url = this.host.getApiBaseURL();
  }

  postUsuarios(request: UsuarioModel): Observable<RespostaServico>{
    return this.http.post<RespostaServico>(this.url + "cadastroUsuario", request);
  }

}