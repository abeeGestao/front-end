import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuarioComponent } from './usuario.component';
import { CreateUsuariosComponent } from './create-usuarios/create-usuarios.component';
import { AlterUsuariosComponent } from './alter-usuarios/alter-usuarios.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [UsuarioComponent, CreateUsuariosComponent, AlterUsuariosComponent],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class UsuarioModule { }
