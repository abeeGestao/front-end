import { UsuarioModel } from './usuario.model';

export interface UsuarioResponseModel {

    quantidadePagina: number;
    listaUsuario: UsuarioModel[];
}
