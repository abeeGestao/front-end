export class UsuarioFiltroRequestModel{
    id: number;
    nomeUsuario: string;
    nome: string;
    perfil: number;
    senha: string
    pagina: number;
    quantidadePagina: number;
}