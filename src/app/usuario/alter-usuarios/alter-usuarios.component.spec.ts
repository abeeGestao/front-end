import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlterUsuariosComponent } from './alter-usuarios.component';

describe('AlterUsuariosComponent', () => {
  let component: AlterUsuariosComponent;
  let fixture: ComponentFixture<AlterUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlterUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlterUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
