import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
import { UsuarioModel } from '../models/usuario.model';
import { ConfigService } from 'app/services/config';

@Injectable({
    providedIn: 'root'
})
export class AlterUsuarioService {
    url : string;
    constructor(private host: ConfigService, private http: HttpClient) { 
      this.url = this.host.getApiBaseURL();
    }

    atualizaUsuario(request: UsuarioModel): Observable<RespostaServico>{
        return this.http.post<RespostaServico>(this.url + "atualizaUsuario", request);
    }
}
