import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
import { CreateUsuariosService } from '../create-usuarios/create-usuarios.service';
import { UsuarioService } from '../usuario.service';
import { AlterUsuarioService } from './alter-usuarios.service';
import { UsuarioFiltroRequestModel } from '../models/usuario-filtro-request.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UsuarioResponseModel } from '../models/usuario-response.model';
import { UsuarioModel } from '../models/usuario.model';
declare var $: any;

@Component({
  selector: 'app-alter-usuarios',
  templateUrl: './alter-usuarios.component.html',
  styleUrls: ['./alter-usuarios.component.css']
})
export class AlterUsuariosComponent implements OnInit {

  requestUsuario: UsuarioFiltroRequestModel = new UsuarioFiltroRequestModel();
  public filtroGetClienteGroup: FormGroup;
  public FormularioAtualizarGroup: FormGroup;

  @Output() respostaAtualizacao = new EventEmitter();

  constructor(public service: CreateUsuariosService,
    public servicoConsulta: UsuarioService,
    public servicoUpdateUsuario: AlterUsuarioService,
    private formBuilder: FormBuilder
    ) { }

  ngOnInit(): void {
    this.requestUsuario.quantidadePagina = 5;
    this.requestUsuario.pagina = 1;

    this.FormularioAtualizarGroup = this.formBuilder.group({
      id: [null],
      nome: [null],
      nomeUsuario: [null],
      senha: [null],
      perfil: [null]
    });
  }

  @Input() set cdUsuarioParam(codigo) {
    this.requestUsuario.id = codigo;
    this.getUsuario();
  }

  getUsuario() {
    this.servicoConsulta.getUsuarios(this.requestUsuario).subscribe(
      (usuarios: UsuarioResponseModel) => {
        const usuario: UsuarioModel = usuarios.listaUsuario[0];
        this.FormularioAtualizarGroup.setValue(usuario);
      }
    );
  }

  public atualizarUsuario() {
    this.servicoUpdateUsuario.atualizaUsuario(this.FormularioAtualizarGroup.value).subscribe(
      (response: RespostaServico) => {
        if (response.codigoServico === 0) {
          this.showNotification('top', 'center', 'success', 'Usuário alterado com sucesso!');
          this.feedback();
        } else {
          this.showNotification('top', 'center', 'warning', 'Falha ao tentar alterar o usuário!');
        }
      }
    );
  }

  feedback() {
    this.respostaAtualizacao.emit({'resposta': 'OK'});
  }

  showNotification(from, align, cor, mensagem) {
    // cor:   '','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: 'notifications',
        message: mensagem

    }, {
        type: cor,
        timer: 50,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }

}
