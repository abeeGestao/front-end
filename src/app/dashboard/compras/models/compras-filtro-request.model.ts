export class ComprasFiltroRequestModel {
    codigo: number;
    nome: string;
    razaoSocial: string;
    total: number;
}