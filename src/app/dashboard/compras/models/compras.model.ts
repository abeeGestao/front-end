export interface ComprasModel {
    codigo: number;
    nome: string;
    razaoSocial: string;
    total: number;
}