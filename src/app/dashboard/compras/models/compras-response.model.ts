import { ComprasModel } from './compras.model';

export interface ComprasResponseModel {
    listaCompras: ComprasModel[];
}
