import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { ComprasFiltroRequestModel } from './models/compras-filtro-request.model';
import { ComprasResponseModel } from './models/compras-response.model';
import { ConfigService } from 'app/services/config';

@Injectable({
    providedIn: 'root'
})
export class ComprasService {
    url : string;
    constructor(private host: ConfigService, private http: HttpClient) { 
      this.url = this.host.getApiBaseURL();
    }
    getDados(request: ComprasFiltroRequestModel): Observable<ComprasResponseModel> {
        return this.http.post<ComprasResponseModel>(this.url + 'totalCompra', request);
    }
}
