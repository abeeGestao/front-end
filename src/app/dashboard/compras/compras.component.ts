import { Component, OnInit } from '@angular/core';
import { ComprasModel } from './models/compras.model';
import { ComprasResponseModel } from './models/compras-response.model';
import { ComprasFiltroRequestModel } from './models/compras-filtro-request.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ComprasService } from './compras.service';

@Component({
  selector: 'app-compras',
  templateUrl: './compras.component.html',
  styleUrls: ['./compras.component.css']
})
export class ComprasComponent implements OnInit {

  compras = {} as ComprasModel;

  comprasResponse: ComprasResponseModel;
  request = {} as ComprasFiltroRequestModel;

  public filtrosGroup: FormGroup;

  constructor(public service: ComprasService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.filtrosGroup = this.formBuilder.group({
      codigo: [null],
      nome: [null],
      razaoSocial: [null],
      total: [null]
    });

    this.getDados();
  }

  getDados() {
    this.service.getDados(this.filtrosGroup.value).subscribe(
      (compras: ComprasResponseModel) => {
        this.comprasResponse = compras;
      }
    );
  }

}
