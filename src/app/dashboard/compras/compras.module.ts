import { ComprasComponent } from './compras.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [],
    exports: [ComprasComponent],
    declarations: [ComprasComponent],
    providers: [],
 })
 
 export class ComprasModule {
 }
