export class DashboardFiltroRequestModel {
    totalProduto: number;
    totalCliente: number;
    totalPedido: number;
    totalVendido: number;
}
