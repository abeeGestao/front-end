import { DashboardModel } from './dashboard.model';

export interface DashboardResponseModel {
    listaDados: DashboardModel[];
}
