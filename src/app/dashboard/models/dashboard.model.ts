export interface DashboardModel {
    totalProduto: number;
    totalCliente: number;
    totalPedido: number;
    totalValorVendido: number;
}
