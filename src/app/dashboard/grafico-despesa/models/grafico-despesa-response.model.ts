import { GraficoDespesaModel } from './grafico-despesa.model';

export interface GraficoDespesaResponseModel {
    listaGrafico: GraficoDespesaModel[];
}
