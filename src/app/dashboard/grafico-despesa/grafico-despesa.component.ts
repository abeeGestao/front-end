import { Component, OnInit } from '@angular/core';
import { GraficoDespesaModel } from './models/grafico-despesa.model';
import { GraficoDespesaResponseModel } from './models/grafico-despesa-response.model';
import { GraficoDespesaFiltroRequestModel } from './models/grafico-despesa-filtro-request.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { GraficoDespesaService } from './grafico-despesa.service';
import Chartist = require('chartist');

@Component({
  selector: 'app-grafico-despesa',
  templateUrl: './grafico-despesa.component.html',
  styleUrls: ['./grafico-despesa.component.css']
})
export class GraficoDespesaComponent implements OnInit {

  grafico = {} as GraficoDespesaModel;

  graficoResponse: GraficoDespesaResponseModel;
  request = {} as GraficoDespesaFiltroRequestModel;

  public filtrosGroup: FormGroup;

  constructor(public service: GraficoDespesaService, private formBuilder: FormBuilder) { }

  startAnimationForLineChart(chart){
    let seq: any, delays: any, durations: any;
    seq = 0;
    delays = 80;
    durations = 500;

    chart.on('draw', function(data) {
      if (data.type === 'line' || data.type === 'area') {
        data.element.animate({
          d: {
            begin: 600,
            dur: 700,
            from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
            to: data.path.clone().stringify(),
            easing: Chartist.Svg.Easing.easeOutQuint
          }
        });
      } else if (data.type === 'point') {
            seq++;
            data.element.animate({
              opacity: {
                begin: seq * delays,
                dur: durations,
                from: 0,
                to: 1,
                easing: 'ease'
              }
            });
        }
    });

    seq = 0;
  };

  startAnimationForBarChart(chart){
    let seq2: any, delays2: any, durations2: any;

    seq2 = 0;
    delays2 = 80;
    durations2 = 500;
    chart.on('draw', function(data) {
      if (data.type === 'bar'){
          seq2++;
          data.element.animate({
            opacity: {
              begin: seq2 * delays2,
              dur: durations2,
              from: 0,
              to: 1,
              easing: 'ease'
            }
          });
      }
    });

    seq2 = 0;
  };

  ngOnInit(): void {

    this.filtrosGroup = this.formBuilder.group({
      total: [null],
      hoje: [null],
      um: [null],
      dois: [null],
      tres: [null],
      quatro: [null],
      cinco: [null],
      seis: [null],
    });

    this.getDados();
    /* ----------==========     Daily Sales Chart initialization For Documentation    ==========---------- */

    
  }

  getDados() {
    this.service.getDados(this.filtrosGroup.value).subscribe(
      (grafico: GraficoDespesaResponseModel) => {
        this.graficoResponse = grafico;
        console.log(this.graficoResponse.listaGrafico[0].total);
        const dataDailySalesChart: any = {
          // tslint:disable-next-line:max-line-length
          labels: ['7º', '6º', '5º', '4º', '3º', '2º', 'Hoje'],
          series: [
              [
                this.graficoResponse.listaGrafico[0].seis,
                this.graficoResponse.listaGrafico[0].cinco,
                this.graficoResponse.listaGrafico[0].quatro,
                this.graficoResponse.listaGrafico[0].tres,
                this.graficoResponse.listaGrafico[0].dois,
                this.graficoResponse.listaGrafico[0].um,
                this.graficoResponse.listaGrafico[0].hoje
              ]
          ]
      };
    
      const optionsDailySalesChart: any = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            // tslint:disable-next-line:max-line-length
            high: this.graficoResponse.listaGrafico[0].total + 5, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
        }
    
        // tslint:disable-next-line:prefer-const
        var dailySalesChart = new Chartist.Line('#dailySalesChartDespesa', dataDailySalesChart, optionsDailySalesChart);
    
        this.startAnimationForLineChart(dailySalesChart);
      }
    );
  }

}
