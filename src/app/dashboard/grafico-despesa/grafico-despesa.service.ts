import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { ConfigService } from 'app/services/config';
import { GraficoDespesaFiltroRequestModel } from './models/grafico-despesa-filtro-request.model';
import { GraficoDespesaResponseModel } from './models/grafico-despesa-response.model';

@Injectable({
    providedIn: 'root'
})
export class GraficoDespesaService {
    url : string;
    constructor(private host: ConfigService, private http: HttpClient) { 
      this.url = this.host.getApiBaseURL();
    }
    getDados(request: GraficoDespesaFiltroRequestModel): Observable<GraficoDespesaResponseModel> {
        return this.http.post<GraficoDespesaResponseModel>(this.url + 'dadosGraficoDespesa', request);
    }
}
