import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficoDespesaComponent } from './grafico-despesa.component';

describe('GraficoDespesaComponent', () => {
  let component: GraficoDespesaComponent;
  let fixture: ComponentFixture<GraficoDespesaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraficoDespesaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficoDespesaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
