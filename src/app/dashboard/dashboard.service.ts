import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { DashboardFiltroRequestModel } from './models/dashboard-filtro-request.model';
import { DashboardResponseModel } from './models/dashboard-response.model';
import { ConfigService } from 'app/services/config';

@Injectable({
    providedIn: 'root'
})
export class DashboardService {
    url : string;
    constructor(private host: ConfigService, private http: HttpClient) { 
      this.url = this.host.getApiBaseURL();
    }
    getDados(request: DashboardFiltroRequestModel): Observable<DashboardResponseModel> {
        return this.http.post<DashboardResponseModel>(this.url + 'totalDados', request);
    }
}
