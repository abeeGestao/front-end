import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { GraficoFiltroRequestModel } from './models/grafico-filtro-request.model';
import { GraficoResponseModel } from './models/grafico-response.model';
import { ConfigService } from 'app/services/config';

@Injectable({
    providedIn: 'root'
})
export class GraficoService {
    url : string;
    constructor(private host: ConfigService, private http: HttpClient) { 
      this.url = this.host.getApiBaseURL();
    }
    getDados(request: GraficoFiltroRequestModel): Observable<GraficoResponseModel> {
        return this.http.post<GraficoResponseModel>(this.url + 'dadosGrafico', request);
    }
}
