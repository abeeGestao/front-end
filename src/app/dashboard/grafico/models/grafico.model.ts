export interface GraficoModel {
    total: number;
    hoje: number;
    um: number;
    dois: number;
    tres: number;
    quatro: number;
    cinco: number;
    seis: number;
}