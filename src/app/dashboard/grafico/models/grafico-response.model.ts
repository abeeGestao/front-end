import { GraficoModel } from './grafico.model';

export interface GraficoResponseModel {
    listaGrafico: GraficoModel[];
}
