import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard.component';
import { ComprasComponent } from './compras/compras.component';
import { GraficoComponent } from './grafico/grafico.component';
import { GraficoDespesaComponent } from './grafico-despesa/grafico-despesa.component';

@NgModule({
  declarations: [DashboardComponent, ComprasComponent, GraficoComponent, GraficoDespesaComponent],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class DashboardModule { }
