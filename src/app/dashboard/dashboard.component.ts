import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';
import { DashboardModel } from './models/dashboard.model';
import { DashboardService } from './dashboard.service';
import { DashboardResponseModel } from './models/dashboard-response.model';
import { DashboardFiltroRequestModel } from './models/dashboard-filtro-request.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  dashboard = {} as DashboardModel;

  dashboardResponse: DashboardResponseModel;
  request = {} as DashboardFiltroRequestModel;

  public filtrosGroup: FormGroup;

  constructor(public service: DashboardService, private formBuilder: FormBuilder) { }

  ngOnInit() {

    this.filtrosGroup = this.formBuilder.group({
      totalProduto: [null],
      totalCliente: [null],
      totalPedido: [null],
      totalVendido: [null]
    });

    this.getDados();

  }

  getDados() {
    this.service.getDados(this.filtrosGroup.value).subscribe(
      (dados: DashboardResponseModel) => {
        this.dashboardResponse = dados;
      }
    );
  }

}
