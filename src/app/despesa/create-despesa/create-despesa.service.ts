import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
import { Observable } from 'rxjs';
import { ConfigService } from 'app/services/config';
import { DespesaModel } from '../models/despesa.model';

@Injectable({
  providedIn: 'root'
})
export class CreateDespesaService {

  url : string;
  constructor(private host: ConfigService, private http: HttpClient) { 
    this.url = this.host.getApiBaseURL();
  }

  postDespesa(request: DespesaModel): Observable<RespostaServico>{
    return this.http.post<RespostaServico>(this.url + "cadastroDespesa", request);
  }

}
