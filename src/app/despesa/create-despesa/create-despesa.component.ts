import { Component, OnInit } from '@angular/core';
import { CreateDespesaService } from './create-despesa.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
declare var $: any;

@Component({
  selector: 'app-create-despesa',
  templateUrl: './create-despesa.component.html',
  styleUrls: ['./create-despesa.component.css']
})
export class CreateDespesaComponent implements OnInit {

  public filtrosGroup: FormGroup;
 
  constructor(public service: CreateDespesaService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.filtrosGroup = this.formBuilder.group({
      descricao: [null],
      data: [null],
      valor: [null]
    });
  }

  public postDespesa() {
    this.service.postDespesa(this.filtrosGroup.value).subscribe(
      (response: RespostaServico) => {
        if (response.codigoServico === 0) {
          this.showNotification('top', 'center', 'success', 'Despesa cadastrada com sucesso!');
        } else {
          this.showNotification('top', 'center', 'warning', 'Falha ao tentar cadastrar a despesa!');
        }
      }
    );
  }

  // Notificação jquery abaixo:
  showNotification(from, align, cor, mensagem) {
    // cor:   '','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: 'notifications',
        message: mensagem

    }, {
        type: cor,
        timer: 50,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }

}
