import { DespesaModel } from './despesa.model';

export interface DespesaResponseModel {
    quantidadePagina: number;
    listaDespesa: DespesaModel[];
}