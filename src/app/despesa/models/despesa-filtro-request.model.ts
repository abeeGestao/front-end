export class DespesaFiltroRequestModel {
    id: number;
    descricao: string;
    dataInicio: Date;
    dataFim: Date;
    valor: number;
    pagina: number;
    quantidadePagina: number;
}