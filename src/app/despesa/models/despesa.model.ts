export interface DespesaModel {
    id: number;
    descricao: string;
    data: Date;
    valor: number;
}
