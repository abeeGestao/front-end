import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
import { ConfigService } from 'app/services/config';
import { DespesaFiltroRequestModel } from './models/despesa-filtro-request.model';
import { DespesaResponseModel } from './models/despesa-response.model';

@Injectable({
    providedIn: 'root'
})
export class DespesaService {
    url : string;
    constructor(private host: ConfigService, private http: HttpClient) {
      this.url = this.host.getApiBaseURL();
    }
    getDespesa(request: DespesaFiltroRequestModel): Observable<DespesaResponseModel> {
        return this.http.post<DespesaResponseModel>(this.url + 'getDespesa', request);
    }

    deleteDespesa(id: number): Observable<RespostaServico> {
        console.log('Requisição Delete, ID:' + id)
        return this.http.delete<RespostaServico>(this.url + 'deleteDespesa/' + id);
    }
}
