import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { DespesaFiltroRequestModel } from '../models/despesa-filtro-request.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CreateDespesaService } from '../create-despesa/create-despesa.service';
import { DespesaService } from '../despesa.service';
import { AlterDespesaService } from './alter-despesa.service';
import { DespesaResponseModel } from '../models/despesa-response.model';
import { DespesaModel } from '../models/despesa.model';
import { RespostaServico } from 'app/clientes/models/resposta-servico.model';
declare var $: any;


@Component({
  selector: 'app-alter-despesa',
  templateUrl: './alter-despesa.component.html',
  styleUrls: ['./alter-despesa.component.css']
})
export class AlterDespesaComponent implements OnInit {

  requestDespesa: DespesaFiltroRequestModel = new DespesaFiltroRequestModel();
  public filtroGetClienteGroup: FormGroup;
  public FormularioAtualizarGroup: FormGroup;

  @Output() respostaAtualizacao = new EventEmitter();

  constructor(public service: CreateDespesaService,
    public servicoConsulta: DespesaService,
    public servicoUpdateDespesa: AlterDespesaService,
    private formBuilder: FormBuilder
    ) { }

  ngOnInit(): void {
    this.requestDespesa.quantidadePagina = 5;
    this.requestDespesa.pagina = 1;
    this.FormularioAtualizarGroup = this.formBuilder.group({
      id: [null],
      descricao: [null],
      data: [null],
      valor: [null]
    });
  }

  @Input() set cdDespesaParam(codigo){
    this.requestDespesa.id = codigo;
    this.getDespesa();
    console.log(codigo);
  }

  getDespesa() {
    this.servicoConsulta.getDespesa(this.requestDespesa).subscribe(
      (despesas: DespesaResponseModel) => {
        const despesa: DespesaModel = despesas.listaDespesa[0];
        this.FormularioAtualizarGroup.setValue(despesa);
      }
    );
  }

  public atualizarDespesa() {
    this.servicoUpdateDespesa.atualizaDespesa(this.FormularioAtualizarGroup.value).subscribe(
      (response: RespostaServico) => {
        if (response.codigoServico === 0) {
          this.showNotification('top', 'center', 'success', 'Despesa alterada com sucesso!');
          this.feedback();
        } else {
          this.showNotification('top', 'center', 'warning', 'Falha ao tentar alterar a despesa!');
        }
      }
    );
  }

  feedback() {
    this.respostaAtualizacao.emit({'resposta':'OK'});
  }

  showNotification(from, align, cor, mensagem) {
    // cor:   '','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: 'notifications',
        message: mensagem

    }, {
        type: cor,
        timer: 50,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }

}
