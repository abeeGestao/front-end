import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlterDespesaComponent } from './alter-despesa.component';

describe('AlterDespesaComponent', () => {
  let component: AlterDespesaComponent;
  let fixture: ComponentFixture<AlterDespesaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlterDespesaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlterDespesaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
